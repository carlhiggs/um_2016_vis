# Urban Monitor data visualisation for Greater Sydney region (2016)

## Problem Statement
The Urban Monitor data is very granular, but requires aggregation to allow meaningful digestion.  It is felt that an interactive web app, allowing consideration of 'any tree %' stratified by public / private / and other land use at LGA and SA1 scales will highlight the rich potential for this data.  We'll find out if we create it and see for ourselves.

## Scope
### Must have:

- interactivity
- LGA aggregation
- public / private /other stratification pie chart

### Nice to have:

- SA1 aggregation
- switch indicators
- data filter